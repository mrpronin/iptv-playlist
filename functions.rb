#!/usr/bin/ruby
#encoding: UTF-8
require 'open-uri'
require 'erb'

def ip2long(ip)
  long = 0
  ip.split(/\./).each_with_index do |b, i|
    long += b.to_i << ( 8*i )
  end
  long
end

def long2ip(long)
  ip = []
  4.times do |i|
    ip.push(long.to_i & 255)
    long = long.to_i >> 8
  end
  ip.join(".")
end

def getPlaylist(link)
	playlist = {}
	begin
#	  file_contents = open('http://iptv.dataline.ua/pl-pis.m3u'){ |f| f.read }
	  file_contents = open(link){ |f| f.read }
#	  ip = file_contents.scan(/(?:[a-z:\/])+@(?:[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})+:(?:[0-9])+\n/)
	  ip = file_contents.scan(/(?:[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})+/)
	  file_contents.scan(/^#EXTINF:0,(.*)$/).each_with_index{|value, key|
	    playlist[ip2long(ip[key])] = value
	  }
	  playlist
	rescue => err
	  puts "Playlist #{link} - #{err}"
	  playlist = {}
	end
end

